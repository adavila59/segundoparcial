# Python
import os
from enum import Enum
from re import template
from typing import List, Optional
import json


# Pydantic
from pydantic import BaseModel, Field
from pydantic import EmailStr, HttpUrl
from pydantic import SecretStr
# FastAPI
from fastapi import FastAPI, Request
from fastapi import Path, Query, Body, Form, File, UploadFile
from fastapi import status
from fastapi import Cookie, Header
from fastapi.responses import FileResponse, HTMLResponse, StreamingResponse
from fastapi import HTTPException

# Nuevos

from fastapi.templating import Jinja2Templates
from fastapi.staticfiles import StaticFiles

app = FastAPI()


###########
# Utilidades

app.mount("/static", StaticFiles(directory="static"), name="static")
templates = Jinja2Templates(directory="templates")


def directory_is_ready():
    os.makedirs(os.getcwd()+"/img", exist_ok=True)
    return os.getcwd()+"/img/"


####


class Program(Enum):
    dbz = "Dragon Ball"
    poke = "Pokemon"
    magia = "senalcolombia"
    cm = "Cuentitos Mágicos"


###########
# Models
class Person(BaseModel):
    nombre: str = Field(...,
                        min_length=3,
                        max_length=20,
                        example="Ernesto")
    actor: bool = Field(...)
    id: int = Field(...,
                    gt=0,
                    example=1)
    email: EmailStr = Field(...)
    web: HttpUrl = Field(...,
                         example="https://www.misenal.tv/")
    age: int = Field(...,
                     gt=0,
                     example=100)


class Character(Person):
    character_name: str = Field(...,
                                min_length=3,
                                max_length=20,
                                example="Frailejon")
    program: Program = Field(default=None, example=Program.cm)


###########
# Primera
@app.get("/",
         tags=["Inicio"],
         summary="Muestra la bienvenida a la API.",
         response_class=HTMLResponse)
def home(request: Request):
    """
    *Muestra la bienvenida a la API.*
        
    """
    context = {'request': request}
    return templates.TemplateResponse("index.html", context)


# Segunda
AboutMe = {
    "About Me": {"nombre": "Alvaro Avila", "Estudiante": True, "Codigo": 67000358, "email": "adavila59@ucatolica.edu.co", "edad": 22, "Personaje Favorito": "Goku", "Programa Favorito": "Dragon Ball"},
}


@app.get(path="/me",
         status_code=status.HTTP_200_OK,
         tags=["Inicio"],
         summary="Muestra los datos del desarrollador.")
def home(
):
    """
    *Muestra los datos del desarrollador.*
        
    """
    return AboutMe


#######
# Personajes
characters = {
    "Goku": {"nombre": "Kakaroto", "actor": False, "id": 1, "email": "www.kakaroto@dbz.com", "web": "http://www.dragonballz.com", "age": 30, "character_name": "Goku", "program": "Dragon Ball"},
    "Ernesto":  {"nombre": "Ernesto", "actor": True, "id": 2, "email": "ernesto@señalcolombia.com", "web": "http://www.cuentosmagicos.org", "age": 6, "character_name": "Frailejon", "program": "Cuentitos Mágicos"}
}


# Tercero
@app.get(path="/character/{nombre}",
         status_code=status.HTTP_200_OK,
         response_model=Character,
         response_model_include=["nombre", id,"email", "character_name", "program"],
         tags=["Character"],
         summary="Mostrar un personaje")
def show_character(
    nombre: str = Path(
        ...
    )
):
    """
    *Mostrar un personaje.*
        
    """
    if nombre not in characters:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail="Character no Registrado")
    return characters[nombre]

# Cuartos


@app.post(path="/character/new",
          status_code=status.HTTP_201_CREATED,
          tags=["Character"],
          response_model=Character,
          summary="Ingresa los datos de un personaje nuevo.")
def new_person(character: Character = Body(...)):
    """
    *Ingresa los datos de un personaje nuevo.*
        
    """
    return character


##########
# IMAGENES
# Quinto
@app.post(path="/character/upload/",
          status_code=status.HTTP_201_CREATED,
          tags=["Images"],
          summary="Recibe y almacena la imagen del personaje.")
async def upload_picture_character(
    image: UploadFile = File(...)
):
    """
    *Recibe y almacena la imagen del personaje.*
        
    """
    dir = directory_is_ready()
    print(dir)
    with open(dir+image.filename, "wb") as myimage:
        content = await image.read()
        myimage.write(content)
        myimage.close()
    return {"Agregado!!"}

# Sexto


@app.get(path="/character/picture/{name}",
         status_code=status.HTTP_200_OK,
         tags=["Images"],
         summary="Muestra la imagen del personaje.")
def show_picture_character(
    name: str = Path(...)
):
    """
    *Muestra la imagen del personaje.*
        
    """
    dir = directory_is_ready()
    path = dir+name+".png"
    if os.path.isfile(path) == False:
        raise HTTPException(status_code=404, detail="No Encontrado")
    return FileResponse(path)


@app.get(path="/answers",
         status_code=status.HTTP_200_OK,
         tags=["Answers"],
         summary="Muestra la respuesta a la pregunta que se le asigna.")
def show_answers():
    """
    *Muestra la respuesta a la pregunta que se le asigna.*
        
    """
    return {"¿Cuál es el método para enviar datos del cliente al servidor?": "Método POST"}


########################################
#JSON 
@app.post(
    path="/store-character",
    response_model=Character,
    summary="Almacenar un Character en un JSON",
    tags=["JSON"],
    status_code=status.HTTP_200_OK
)
def store_user(character:Character=Body(...)):
    """
    Almacenar un character en un JSON
    """
    
    with open("characters.json", "r+") as file:
        current = json.loads(file.read())
        aux = list(current)
        character_dict = character.dict()
        aux.append(character_dict)
        file.seek(0)
        file.write(json.dumps(aux))
        file.close()
    return character

@app.get(
    path="/user/list", 
    status_code=status.HTTP_200_OK,
    response_model=List[Character],
    summary="Leer Character en un JSON",
    tags=["JSON"],
)
def list_all_users():
    with open("characters.json", "r+", encoding="utf-8") as file:
        current = json.loads(file.read())
    return current